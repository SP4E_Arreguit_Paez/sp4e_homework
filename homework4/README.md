Code by Laura Paez and Jonathan Arreguit

# Homework 4

## Make instructions

Compile the controller:

1. `cd PATH_TO_SP4E_Arreguit_Paez/homework4/starting_point`
2. `mkdir build`
3. `cd build/`
4. `cmake ..`
5. `make`

## Run instructions

In order to run the python scripts, you must first follow the make instructions
above. Once that is done, you can for example run the _main.py_ script given with
the homework as:

1. `mkdir dumps`
2. `python3 main.py 1000 10 heat.csv "material_point" 1e-2`

Note that this requires the compiled pypart binaries (typically called
_libpart.so_ and _pypart.cpython-36m-x86\_64-linux-gnu.so_) to be placed in the
working directory with _main.py_. You will also need an acceptable heat.csv file
in your working directory to run this command. An example of heat.csv file can
be found in `PATH_TO_SP4E_Arreguit_Paez/homework4/starting_point`.  After
launching the `main.py` script, the simulation logs will be written to the dumps
folder. Please refer to `python3 main.py -h` for additional information about
the arguments.

Some additionally scripts have also been implemented to quickly run
simulation. They can be run if the make instructions were followed correctly and
the following steps are followed.

1. `cd PATH_TO_SP4E_Arreguit_Paez/homework4/starting_point`
2. `python3 run_simulation.py`

and 

1. `cd PATH_TO_SP4E_Arreguit_Paez/homework4/starting_point`
2. `python3 test_simulation.py material_point`

These scripts directly add the build folder to the pythonpath internally. You
can get more information about these scripts by running `python3
run_simulation.py -h` and `python3 test_simulation.py -h` respectfully.

## Exercise 1 comments

In class MaterialPointsFactory, the createSimulation function has been
overloaded to take functor as one of its argument. It is overloaded in such a
way that custom computations can be added to the simulation steps. In the case
of _main.py_, a python createComputes function is provided and is called when the
material_points_factory creates the simulation. A ComputeTemperature computation
is called directly from Python and is modified with custom options such that it
can then be added to the computation steps of the simulation. Other computations
could also be added here. Note how this can thus be done in Python once at
initialisation without requiring a recompilation of the code, making it simple
to test different scenarios.

## Exercise 2 comments

In order to ensure that references to Compute objects type are correctly managed
in the python bindings, one must insure that Python does not remove data which
is still referenced within the C++ code, typically due to the garbage collector
when the number of Python’s reference to a Python object reaches zero, though
pointers from C++ could still be pointing to the data. To avoid this kind of
issue, pybind11 support handling of shared pointers as explained in its
[documentation](https://pybind11.readthedocs.io/en/stable/advanced/smart_ptrs.html?highlight=std%3A%3Ashared_ptr),
meaning that the Python interpreter will not delete the data unless it is not
referenced in C++ anymore either. As they explain, _"the binding generator for a
class can be passed a template type that denotes a special holder type that is
used to manage references to the object"_. As such, this was done for the
Compute and ComputeTemperature objects which can be found in
_pypart.cc_.

Additionally, the _main.py_ script makes use of private class variables. Thus
properties handling was also integrated to the ComputeTemperature class bindings
such that python could easily interface with the class variables without needing
to make function calls. Note that this required the implementation of new
appropriate methods in the ComputeTemperature class which can be found in
_compute_temperature.hh_.

#!/bin/env python3

""" A script for running simulations with pypart """

import os
import sys
import argparse
sys.path.insert(0, './build')
from pypart import MaterialPointsFactory, ParticlesFactoryInterface
from pypart import PingPongBallsFactory, PlanetsFactory
from pypart import CsvWriter
from pypart import ComputeTemperature
# help(pypart)


class SimulationOptions:
    """ Simulation options """

    def __init__(self, **kwargs):
        super(SimulationOptions, self).__init__()
        self.nsteps = kwargs.pop("nsteps", 1000)
        self.freq = kwargs.pop("freq", 10)
        self.filename = kwargs.pop("filename", "heat.csv")
        self.particle_type = kwargs.pop("particle_type", "material_point")
        self.timestep = kwargs.pop("timestep", 0.01)

    def dump_dict(self):
        """ Dumpt options to list """
        return {
            "nsteps": self.nsteps,
            "freq": self.freq,
            "filename": self.filename,
            "particle_type": self.particle_type,
            "timestep": self.timestep
        }

    def dumps(self, sep="\n"):
        """ Dump options to string """
        items = self.dump_dict()
        return sep.join(
            ["{}: {}".format(i, items[i]) for i in items]
        )


def simulation_command_line_options():
    """ Parse command line arguments """
    parser = argparse.ArgumentParser(description='Particles code')
    parser.add_argument(
        '--nsteps', type=int,
        default=100,
        help='specify the number of steps to perform')
    parser.add_argument(
        '--freq', type=int,
        default=10,
        help='specify the frequency for dumps')
    parser.add_argument(
        '--filename', type=str,
        default="heat.csv",
        help='start/input filename')
    parser.add_argument(
        '--particle_type', type=str,
        default="material_point",
        help='particle type')
    parser.add_argument(
        '--timestep', type=float,
        default=0.01,
        help='timestep')
    args = parser.parse_args()
    return SimulationOptions(
        nsteps=args.nsteps,
        freq=args.freq,
        filename=args.filename,
        particle_type=args.particle_type,
        timestep=args.timestep
    )


def create_computes(self, _timestep):
    """ Create computes """
    compute_temp = ComputeTemperature()
    compute_temp.conductivity = 1
    compute_temp.L = 2
    compute_temp.capacity = 1
    compute_temp.density = 1
    compute_temp.deltat = 1
    self.system_evolution.addCompute(compute_temp)


def run_simulation(options):
    """ Run simulation """
    print("Running simulation with options:\n  {}".format(
        options.dumps(sep="\n  ")
    ))

    if options.particle_type == "planet":
        PlanetsFactory.getInstance()
    elif options.particle_type == "ping_pong":
        PingPongBallsFactory.getInstance()
    elif options.particle_type == "material_point":
        MaterialPointsFactory.getInstance()
    else:
        print("Unknown particle type: {}".format(options.particle_type))
        sys.exit(-1)

    factory = ParticlesFactoryInterface.getInstance()

    if options.particle_type == "material_point":
        evol = factory.createSimulation(
            options.filename,
            options.timestep,
            create_computes
        )
    else:
        evol = factory.createSimulation(
            options.filename,
            options.timestep
        )

    dumper = CsvWriter("init.csv")
    dumper.write(evol.getSystem())
    dumps_folder = "dumps"
    if dumps_folder not in os.listdir():
        os.makedirs(dumps_folder)

    evol.setNSteps(options.nsteps)
    evol.setDumpFreq(options.freq)
    evol.evolve()
    print("Simulation complete")


if __name__ == '__main__':
    OPTIONS = simulation_command_line_options()
    run_simulation(OPTIONS)

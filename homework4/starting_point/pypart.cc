#include <functional>
#include <iostream>
#include <pybind11/pybind11.h>
#include "particles_factory_interface.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"
#include "csv_writer.hh"
#include "compute_temperature.hh"


namespace py = pybind11;


PYBIND11_MODULE(pypart, m) {
    m.doc() = "pybind particles";

    //bind of ParticlesFactoryInterface class
    py::class_<ParticlesFactoryInterface>(m, "ParticlesFactoryInterface")
        .def(
            "getInstance",
            &ParticlesFactoryInterface::getInstance,
            py::return_value_policy::reference)
        .def(
            "createSimulation",
            &ParticlesFactoryInterface::createSimulation);

    //bind of MaterialPointsFactory class
    py::class_<MaterialPointsFactory>(m, "MaterialPointsFactory")
        .def(
            "getInstance",
            &MaterialPointsFactory::getInstance,
            py::return_value_policy::reference)
        .def(
            "createSimulation",
            (SystemEvolution & (MaterialPointsFactory::*)(const std::string &, Real)) & // cast
            MaterialPointsFactory::createSimulation) // method pointer
        .def(
            "createSimulation",
            py::overload_cast<const std::string &, Real, py::function>(
                &MaterialPointsFactory::createSimulation<py::function>),
            py::return_value_policy::reference)
        .def_property_readonly(
            "system_evolution",
            &MaterialPointsFactory::getSystemEvolution);

    //bind of PingPongBallsFactory class
    py::class_<PingPongBallsFactory>(m, "PingPongBallsFactory")
        .def(
            "getInstance",
            &PingPongBallsFactory::getInstance,
            py::return_value_policy::reference)
        .def(
            "createSimulation",
            &PingPongBallsFactory::createSimulation,
            py::return_value_policy::reference)
        .def_property_readonly(
            "system_evolution",
            &PingPongBallsFactory::getSystemEvolution);

    //bind of PlanetsFactory class
    py::class_<PlanetsFactory>(m, "PlanetsFactory")
        .def(
            "getInstance",
            &PlanetsFactory::getInstance,
            py::return_value_policy::reference)
        .def(
            "createSimulation",
            &PlanetsFactory::createSimulation,
            py::return_value_policy::reference)
        .def_property_readonly(
            "system_evolution",
            &PlanetsFactory::getSystemEvolution);

    //bind of CsvWriter class
    py::class_<CsvWriter>(m, "CsvWriter")
        .def(
            py::init<const std::string&>())
        .def(
            "write",
            &CsvWriter::write);

    py::class_<Compute, std::shared_ptr<Compute>>(m, "Compute");

    //bind of ComputeTemperature class
    py::class_<ComputeTemperature, Compute, std::shared_ptr<ComputeTemperature>>(m, "ComputeTemperature")
        .def(
            py::init<>())
        .def_property(
            "conductivity",
            &ComputeTemperature::getConductivity,
            &ComputeTemperature::setConductivity)
        .def_property(
            "L",
            &ComputeTemperature::getL,
            &ComputeTemperature::setL)
        .def_property(
            "capacity",
            &ComputeTemperature::getCapacity,
            &ComputeTemperature::setCapacity)
        .def_property(
            "density",
            &ComputeTemperature::getDensity,
            &ComputeTemperature::setDensity)
        .def_property(
            "deltat",
            &ComputeTemperature::getDeltat,
            &ComputeTemperature::setDeltat);

    //bind of System class
    py::class_<System>(m, "System");

    //bind of SystemEvolution
    py::class_<SystemEvolution>(m, "SystemEvolution")
        .def(
            "addCompute",
            &SystemEvolution::addCompute)
        .def(
            "getSystem",
            &SystemEvolution::getSystem,
            py::return_value_policy::reference)
        .def(
            "setNSteps",
            &SystemEvolution::setNSteps)
        .def(
            "setDumpFreq",
            &SystemEvolution::setDumpFreq)
        .def(
            "evolve",
            &SystemEvolution::evolve);
}

#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"

//! Compute contact interaction between ping-pong balls
class ComputeTemperature : public Compute {
  
public:
  //! temperature evolution implementation
  void compute(System& system) override;

  //! return the heat conductivity
  Real & getConductivity(){return conductivity;};
  //! return the heat capacity
  Real & getCapacity(){return capacity;};
  //! return the heat capacity
  Real & getDensity(){return density;};
  //! return the characteristic length of the square
  Real & getL(){return L;};
  //! return the characteristic length of the square
  Real & getDeltat(){return delta_t;};

  //functions created to make main.py work:
  //! set the heat conductivity
  void setConductivity(Real conductivity){this->conductivity=conductivity;};
  //! set the heat capacity
  void setCapacity(Real capacity){this->capacity=capacity;};
  //! set the heat density
  void setDensity(Real density){this->density=density;};
  //! set the L
  void setL(Real L){this->L=L;};
  //! set delta_t
  void setDeltat(Real delta_t){this->delta_t=delta_t;};


  bool implicit = true;
private:
  
  Real conductivity;
  Real capacity;
  Real density;
  //! side length of the problem
  Real L;

  Real delta_t;
};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__

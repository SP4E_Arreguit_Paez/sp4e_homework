#!/bin/env python3

""" Test simulation """

import argparse
from run_simulation import SimulationOptions, run_simulation


def simulation_command_line_options():
    """ Parse command line arguments """
    parser = argparse.ArgumentParser(description='Particles code')
    parser.add_argument(
        type=str,
        default="material_point",
        dest="particle_type",
        help="particle type can be planet, ping_pong or material_point")
    args = parser.parse_args()
    return SimulationOptions(
        nsteps=3,
        freq=1,
        filename=(
            "planets.csv" if args.particle_type == "planet"
            else "ping_pong.csv" if args.particle_type == "ping_pong"
            else "heat.csv"
        ),
        particle_type=args.particle_type,
        timestep=0.1
    )


def main():
    """ Main - Test simulation for different options """
    options = simulation_command_line_options()
    run_simulation(options)


if __name__ == '__main__':
    main()

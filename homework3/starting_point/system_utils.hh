#ifndef __SYSTEM_UTILS__HH__
#define __SYSTEM_UTILS__HH__

#include "fft.hh"
#include "material_point.hh"
#include <cmath>

/* -------------------------------------------------------------------------- */
// Create new function which separates
// Use landa instead
inline void system_read(MaterialPoint& particle, Real& (MaterialPoint::*method)(), complex& val)
{
    val = (particle.*method)();
}
inline void system_write(MaterialPoint& particle, Real& (MaterialPoint::*method)(), complex& val) {
    (particle.*method)() = std::real(val);
}
/* -------------------------------------------------------------------------- */
inline Matrix<complex> matrix_op(System& system, Matrix<complex>& matrix, Real& (MaterialPoint::*method)(), void (*function)(MaterialPoint&, Real& (MaterialPoint::*method)(), complex&)) {
    // auto n_particles = system.getNbParticles();
    auto square_side = matrix.rows();
    // Matrix<complex> matrix(square_side);
    auto particle_index = 0;
    for (auto&& entry : index(matrix)) {
        auto i = std::get<0>(entry);
        auto j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        particle_index = i*square_side + j;
        auto& particle = static_cast<MaterialPoint&>(
            system.getParticle(particle_index));
        function(particle, method, val);
    }
    return matrix;
}
/* -------------------------------------------------------------------------- */
inline void extract_to_matrix(System& system, Matrix<complex>& matrix, Real& (MaterialPoint::*method)()) {
    matrix_op(system, matrix, method, system_read);
}
/* -------------------------------------------------------------------------- */
inline void set_from_matrix(System& system, Matrix<complex>& matrix, Real& (MaterialPoint::*method)()) {
    matrix_op(system, matrix, method, system_write);
}
/* -------------------------------------------------------------------------- */
inline complex matrix_average(Matrix<complex>& matrix) {
    Real average = 0;
    auto n_particles = matrix.rows()*matrix.cols();
    for (auto&& entry : index(matrix)) {
        auto i = std::get<0>(entry);
        auto j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        average += std::real(val);
        if (!(i%20) && !(j%20))
            std::cout << i << ", " << j << ": " << val << std::endl;
    }
    return average / n_particles;
}
/* -------------------------------------------------------------------------- */

#endif  //__SYSTEM_UTILS__HH__

""" Run simulation """

import subprocess
import gen_heat


class Simulation:
    """ Run simulation """

    def __init__(self, filename=None):
        super(Simulation, self).__init__()
        self.filename = filename
        self.exe = "./build/particles"
        self.nsteps = 1000
        self.dump_freq = 10
        self.particle_type = "material_point"
        self.timestep = 1e-2

    def generate(self):
        """ Generate data before simulation """
        gen_heat.generate_heat_distribution(512, 512, self.filename)

    def run(self):
        """ Run simulation """
        command = "{} {} {} {} {} {}".format(
            self.exe,
            self.nsteps,
            self.dump_freq,
            self.filename,
            self.particle_type,
            self.timestep
        )
        print(command)
        subprocess.run(command.split())

    def view(self):
        """ View simulation results """
        print("View results of simulation {}".format(self.filename))


def main():
    """ Main - run simulation """
    simulation = Simulation(filename="./dumps/heat.csv")
    simulation.generate()
    simulation.run()
    simulation.view()
    return simulation

if __name__ == '__main__':
    main()

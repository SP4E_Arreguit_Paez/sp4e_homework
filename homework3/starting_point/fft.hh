#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>
/* ------------------------------------------------------ */

struct FFT {
  static Matrix<complex> transform(Matrix<complex>& m);
  static Matrix<complex> itransform(Matrix<complex>& m);
  static Matrix<complex> computeFrequencies(int size);

};

/* ------------------------------------------------------ */
/**
 *  \brief Compute the Fourier Transform
 *
 *  Detailed description
 *
 *  \param m_in Input matrix
 *  \return The Fourier Transform matrix
 */
inline Matrix<complex> FFT::transform(Matrix<complex>& m_in) {

    int N = m_in.rows();
    Matrix<complex> m_out(N);
    fftw_plan my_plan;

    //casting
    complex *data_in = m_in.data();
    complex *data_out = m_out.data();
    auto *in = reinterpret_cast<fftw_complex *>( data_in);
    auto *out = reinterpret_cast<fftw_complex *>( data_out);

    //assign, execute and destroy my plan
    my_plan = fftw_plan_dft_2d(N, N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(my_plan);
    fftw_destroy_plan(my_plan);

    return m_out;
}

/* ------------------------------------------------------ */
/**
 *  \brief Compute the Inverse Fourier Transform
 *
 *  \param m_in Input matrix
 *  \return The Inverse Fourier Transform matrix
 */
inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {

    int N = m_in.rows();
    Matrix<complex> m_out(N);
    fftw_plan my_plan;

    //casting
    std::complex<double> *data_in = m_in.data();
    std::complex<double> *data_out = m_out.data();
    auto *in = reinterpret_cast<fftw_complex *>( data_in);
    auto *out = reinterpret_cast<fftw_complex *>( data_out);

    //assign, execute and destroy my plan
    my_plan = fftw_plan_dft_2d(N, N, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(my_plan);
    fftw_destroy_plan(my_plan);

    //Divide resultant matrix by N square
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            m_out(i, j) /= (N * N);
        }
    }
    return m_out;
}

/* ------------------------------------------------------ */
/**
 *  \brief Compute the Discrete Fourier Transform sample frequencies.
 *
 *  \param size The sample size of the signal (Window length)
 *  \return Matrix<complex> The sample frequency matrix
 */
inline Matrix<complex> FFT::computeFrequencies(int size) {
//the matrix was changed from int to float!
    Matrix<complex> frequency_matrix(size);
    int N = (size-1)/2 + 1;
    int step=0;
    int odd=0;
    if (size % 2 == 0) { // size is even
        step= (N-1)/((size/2)-1);
        odd=0;
    }
    else{ // size is odd
        step= (N-1)/(size/2);
        odd=1;
    }

    //computation of the first part of the frequencies
    for (int i = 0; i < size; ++i) {
        for (int j = 1; j < size/2+odd; ++j) {
            frequency_matrix(i, j) = (j*step*1.)/size;
        }
    }

    //computation of the second part of the frequencies
    for (int i = 0; i < size; ++i) {
        int temp = 0;
        for (int j = size/2+odd; j < size; ++j) {
            frequency_matrix(i, j) = (-size/2+temp*step*1.)/size;
            temp++;
        }
    }

    return frequency_matrix;

}

#endif  // FFT_HH

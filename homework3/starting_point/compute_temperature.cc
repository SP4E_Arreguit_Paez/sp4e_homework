#include "compute_temperature.hh"
#include "system_utils.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>

/* -------------------------------------------------------------------------- */
void ComputeTemperature::compute(System& system) {
    auto n_particles = system.getNbParticles();
    auto square_side = sqrt(n_particles);
    // Temperatures
    Matrix<complex> temperature_matrix(square_side);
    extract_to_matrix(system, temperature_matrix, &MaterialPoint::getTemperature);
    auto temperature_fft = FFT::transform(temperature_matrix);
    std::cout << "Computing average system temperature" << std::endl;
    auto temp = matrix_average(temperature_matrix);
    std::cout << "Average system temperature: " << temp << std::endl;
    // Heat rate
    Matrix<complex> heat_rate_matrix(square_side);
    extract_to_matrix(system, heat_rate_matrix, &MaterialPoint::getHeatRate);
    auto heat_rate_fft = FFT::transform(heat_rate_matrix);

    // Copper properties
    Real rho = 8.96; // Copper mass density
    Real C = 0.385; // Copper specific heat capacity
    Real kappa = 385.0; // Copper heat capacity

    // Compute step
    auto dt = system.getTimestep();
    auto freqs = FFT::computeFrequencies(square_side);
    auto div = 1./(rho*C);
    for (auto&& entry : index(temperature_fft)) {
        auto i = std::get<0>(entry);
        auto j = std::get<1>(entry);
        auto& point_temperature = std::get<2>(entry);
        auto qx2 = pow(std::real(freqs(0, i)), 2);
        auto qy2 = pow(std::real(freqs(0, j)), 2);
        auto dtemp_dtime = heat_rate_fft(i, j);
        dtemp_dtime -= kappa*point_temperature*(qx2 + qy2);
        dtemp_dtime *= div;
        point_temperature += dt*dtemp_dtime;
    }

    // Set temperature new values
    temperature_matrix = FFT::itransform(temperature_fft);

    // Boundary conditions
    for (UInt i = 0; i < square_side; i++)
    {
        temperature_matrix(i, 0) = 0;
        temperature_matrix(0, i) = 0;
        temperature_matrix(i, square_side-1) = 0;
        temperature_matrix(square_side-1, i) = 0;
    }

    // Set temperature to system particles
    set_from_matrix(system, temperature_matrix, &MaterialPoint::getTemperature);
}
/* -------------------------------------------------------------------------- */

#include "compute_gravity.hh"
#include "compute_verlet_integration.hh"
#include "csv_reader.hh"
#include "csv_writer.hh"
#include "my_types.hh"
#include "ping_pong_balls_factory.hh"
#include "material_points_factory.hh"
#include "planets_factory.hh"
#include "system.hh"
#include <fftw3.h>
#include "system_utils.hh"
/* -------------------------------------------------------------------------- */
#include <cstdlib>
#include <iostream>
#include <sstream>
/* -------------------------------------------------------------------------- */
#include <gtest/gtest.h>

/*****************************************************************/
TEST(COMPUTE_TEMPERATURE, no_gradient) {
    // the number of steps to perform
    Real nsteps = 1;
    // freq to dump
    int freq = 10;
    // init file
    std::string filename = "./../tests/test_no_gradient.csv";
    // particle type
    std::string type = "material_point";
    // timestep
    Real timestep = 1e-2;

    MaterialPointsFactory::getInstance();
    ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();

    SystemEvolution& evol = factory.createSimulation(filename, timestep);
    evol.setNSteps(nsteps);
    evol.setDumpFreq(freq);
    auto system = evol.getSystem();
    auto n_particles = system.getNbParticles();
    auto square_side = sqrt(n_particles);
    Matrix<complex> temperature_matrix_t0(square_side);
    extract_to_matrix(system, temperature_matrix_t0, &MaterialPoint::getTemperature);
    evol.evolve();
    Matrix<complex> temperature_matrix_t1(square_side);
    extract_to_matrix(system, temperature_matrix_t1, &MaterialPoint::getTemperature);
    for (auto&& entry : index(temperature_matrix_t1)) {
        auto i = std::get<0>(entry);
        auto j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        auto temperature_diff = std::abs(val - temperature_matrix_t0(i, j));
        ASSERT_NEAR(temperature_diff, 0, 1e-6);
    }
}
/*****************************************************************/
TEST(COMPUTE_TEMPERATURE, volumetric_heat_source) {
    // the number of steps to perform
    Real nsteps = 1;
    // freq to dump
    int freq = 10;
    // init file
    std::string filename = "./../tests/test_volumetric_heat_source.csv";
    // particle type
    std::string type = "material_point";
    // timestep
    Real timestep = 1e-2;

    MaterialPointsFactory::getInstance();
    ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();

    SystemEvolution& evol = factory.createSimulation(filename, timestep);
    evol.setNSteps(nsteps);
    evol.setDumpFreq(freq);
    auto system = evol.getSystem();
    auto n_particles = system.getNbParticles();
    auto square_side = sqrt(n_particles);
    Matrix<complex> temperature_matrix_t0(square_side);
    extract_to_matrix(system, temperature_matrix_t0, &MaterialPoint::getTemperature);
    evol.evolve();
    Matrix<complex> temperature_matrix_t1(square_side);
    extract_to_matrix(system, temperature_matrix_t1, &MaterialPoint::getTemperature);
    for (auto&& entry : index(temperature_matrix_t1)) {
        auto i = std::get<0>(entry);
        auto j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        auto temperature_diff = std::abs(val - temperature_matrix_t0(i, j));
        ASSERT_NEAR(temperature_diff, 0, 1e-6);
    }
}
/*****************************************************************/

""" Generate heat distribution CSV file """

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm


def function(n_points):
    """ Function """
    length = 2
    temperature = np.zeros((n_points, n_points))
    heat_rate = np.zeros((n_points, n_points))
    heat_rate = np.array([
        [
            1 if (x - 0.5)**2 < 1e-4
            else -1 if (x + 0.5)**2 < 1e-4
            else 0
            for x in np.linspace(-1, 1, n_points)
        ]
        for y in np.linspace(-1, 1, n_points)
    ])
    heat_rate[int(n_points/4), :] = -1*150
    heat_rate[int(3*n_points/4), :] = 1*150
    freqs = np.fft.fftfreq(n_points)*2*np.pi/length*n_points
    freqs_2d = np.einsum('i,j->ij', np.ones(n_points), freqs)
    freqs = freqs_2d**2 + freqs_2d.T**2
    # freqs[0, 0] = 1.
    q_hat = np.fft.fft2(heat_rate)
    temperature_hat = q_hat/freqs
    temperature_hat[0, 0] = 0.
    temperature = np.fft.ifft2(temperature_hat)
    return temperature


def plot_function():
    """ Plot function """
    n_points = 100
    temperature = function(n_points)
    plt.imshow(temperature.real)
    fig = plt.figure()
    plot_ax = fig.gca(projection='3d')
    x_samples = np.linspace(-1, 1, n_points)
    x_2d_samples = np.einsum('i,j->ij', np.ones(n_points), x_samples)
    surf = plot_ax.plot_surface(
        x_2d_samples,
        x_2d_samples.T,
        temperature.real,
        cmap=cm.coolwarm,
        antialiased=False
    )
    plt.show()


def generate_heat_distribution(size_x=512, size_y=512, filename="heat.csv"):
    """Generate heat distribution

    This function generated a heat distribution according to:

    h(x, y) = 1 if x**2 + y**2 < 1 else 0

    and places the final result inside a CSV file provided by the filename in
    the following order:

    position3 velocity3 force3 mass1 temperature1 heat_rate1

    """
    position_z = 0
    velocity = [0, 0, 0]
    force = [0, 0, 0]
    rho = [1]
    # temperature = function(size_x).real
    config = np.array(
        [
            [x, y, position_z]  # Position
            + velocity
            + force
            + rho
            + [25 if (x**2 + y**2 < 1) else 0]  # Temperature
            + [0 if (x**2 + y**2 < 1) else 0]  # Volumetric heat conductivity
            # + [temperature[i, j]]  # Temperature
            # + [
            #     1 if (x - 0.5)**2 < 1e-4
            #     else -1 if (x + 0.5)**2 < 1e-4
            #     else 0
            # ]  # Volumetric heat conductivity
            for i, x in enumerate(np.linspace(-1, 1, size_x))
            for j, y in enumerate(np.linspace(-1, 1, size_y))
        ]
    )
    np.savetxt(filename, config, fmt='%.6f')


if __name__ == "__main__":
    generate_heat_distribution(512, 512)
    plot_function()

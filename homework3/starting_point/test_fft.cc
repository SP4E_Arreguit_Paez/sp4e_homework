#include "my_types.hh"
#include "fft.hh"
#include <gtest/gtest.h>

/*****************************************************************/
/**
 *  \brief Test of the forward FFT
 *
 *  This test consists in testing the fourier transform of f(x) = cos(k*x) with
 *  k = 2 * M_PI / N and N = 521.
 *
 */
TEST(FFT, transform) {
  UInt N = 512;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
      val = cos(k * i);
  }

  Matrix<complex> res = FFT::transform(m);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (std::abs(val) > 1e-10)
      std::cout << i << "," << j << " = " << val << std::endl;

    if (i == 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else if (i == N - 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else
      ASSERT_NEAR(std::abs(val), 0, 1e-10);
  }

}
/*****************************************************************/
/**
 *  \brief Test of the backward FFT (iFFT)
 *
 *  This test runs the inverse of the previous test, where we have an FFT matrix
 *  M = NxN with M[1, 0] = N*N/2, M[N-1, 0] = N*N/2 and M[i, j] = 0
 *  otherwise. The solution to the iFFT of this matrix should thus be f(i) =
 *  cos(3*i).
 *
 */
TEST(FFT, inverse_transform) {
    UInt N = 512;
    Matrix<complex> m(N);

    Real k = 2 * M_PI / N;
    for (auto&& entry : index(m)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        if ((i == 1 && j == 0) || (i == N - 1 && j == 0)) {
            val = N * N / 2;
        } else {
            val = 0;
        }
    }

    Matrix<complex> res = FFT::itransform(m);

    for (auto&& entry : index(res)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        ASSERT_NEAR(std::abs(val), std::abs(cos(k * i)), 1e-6);
    }
}
/*****************************************************************/
/**
 *  \brief Test of the frequencies for an even sample number
 */
TEST(FFT, frequencies_even) {
    UInt N = 4;
    Matrix<complex> frequencies = FFT::computeFrequencies(N);
    for (auto&& entry : index(frequencies)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        // std::cout << i << ", " << j << ": " << val << std::endl;
        if (j == 0)
            ASSERT_NEAR(std::real(val), 0, 1e-6);
        else if (j == 1)
            ASSERT_NEAR(std::real(val), 0.25, 1e-6);
        else if (j == 2)
            ASSERT_NEAR(std::real(val), -0.5, 1e-6);
        else if (j == 3)
            ASSERT_NEAR(std::real(val), -0.25, 1e-6);
    }
}

/**
 *  \brief Test of the frequencies for an odd sample number
 */
TEST(FFT, frequencies_odd) {
    UInt N = 5;
    Matrix<complex> frequencies = FFT::computeFrequencies(N);
    for (auto&& entry : index(frequencies)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        // std::cout << i << ", " << j << ": " << val << std::endl;
        if (j == 0)
            ASSERT_NEAR(std::real(val), 0, 1e-6);
        else if (j == 1)
            ASSERT_NEAR(std::real(val), 0.2, 1e-6);
        else if (j == 2)
            ASSERT_NEAR(std::real(val), 0.4, 1e-6);
        else if (j == 3)
            ASSERT_NEAR(std::real(val), -0.4, 1e-6);
        else if (j == 4)
            ASSERT_NEAR(std::real(val), -0.2, 1e-6);
    }
}
/*****************************************************************/

Code by Laura Paez and Jonathan Arreguit

# Homework 3

## Library installation:

Install the FFTW library by running the next command:

`sudo apt install libfftw3-dev`

## Make instructions

Compile the controller:

1. `cd PATH_TO_SP4E_Arreguit_Paez/homework3/starting_point`
2. `mkdir build`
3. `cd build/`
4. `cmake ..`
5. `make`

## Run instructions

In order to create the input file and run the homework, use the following command:

1. `cd PATH_TO_SP4E_Arreguit_Paez/homework3/starting_point`
2. `python3 run.py`

## Code discovery (Exercise 1)

* **MaterialPoint**: Derived class from Particle class that adds the temperature and heat rate values. Attributes that are used in order to compute the heat equation.
* **Matrix**: In the code a particle is a point in a grid, and the grid is represented using a matrix. The Matrix structure contains the operations (i.e. operators like * or ++) and information related with a matrix (i.e. size).
* **MaterialPointsFactory**: The material particles and its simulation are created in this class. During the simulation the spatio temporal evolution of the temperature (i.e. heat equation) is computed along the particles.
* **FFT**: Wrapping interface for the FFTW3 library. It allows the computation of forward and inverse Fourier Transform. And also the Discrete Fourier Transform sample frequencies.

## Boundary condition addition (Exercise 4.4):

Inside compute_temperature.cc (where the temperature is computed), after the computational step and once the new temperature values are set, the boundary condition is located. It consists in assign a zero value temperature to the borders.

## Paraview instrucions (Exercise 4.5)

In order to visualize the results in paraview there are two ways:

Set all the parameters in paraview manually:

1. Open Paraview.
2. Open the resultant (step) .csv files located in the dumps folder.
3. Set delimiter as from "," to " " (space).
4. Deselect the headers option and select "Apply".
5. Press Ctrl+space and use the option: Tableto Points.
6. Set x column as Field 0 and y column as Field 1.
7. Select 2D points and press "Apply".
8. Click inside the render view and within the pipeline browser activate the visibility icon (i.e. eye sketch).
9. Set the representation from surface to points.
10. Within coloring select Filed 13 (it is the equivalent to temperature).
11. Within styling set the point size to 7.
12. Press play.

Use a state file that contains a pre-established configuration:
1. Open Paraview.
2. Open the state file: file -> Load State and select the file PreEstablishedStateParaview.pvsm within the dumps folder(PATH_TO_SP4E_Arreguit_Paez/homework3/starting_point/dumps)
3. Select the resultant (step) .csv files located in the dumps folder.
4. Press play.

The paraview simulation results for a grid of 512 x 512 particles can be seen below (click on the image):
[![](http://img.youtube.com/vi/L_kDf3ySENU/0.jpg)](http://www.youtube.com/watch?v=L_kDf3ySENU "Heat equation result")

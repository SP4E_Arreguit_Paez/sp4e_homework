# SP4E homework

**Student names:** Laura Paez & Jonathan Patrick Arreguit O'Neill

## Instructions for running the codes

### Homework 1

The full homework can be run by running a console in the
**sp4e_homework/Homework1** directory and running the following command:

`python homework1.py`

Exercise 1 and 2 can also be run independently by running 

- Exercise 1 (optimizer): `python optimizer.py`
- Exercise 2 (conjugate gradient): `python conjugate_gradient.py`

The code is expected to be compatible with both python 2.7+ and python
3.4+. Note that running exercise 2 independently will not feature the plots of
the results implemented in exercise 1.

The results obtained were the following:

**BFGS:**

![IMGBFGS2D](Homework_1/Results/BFGS_2d.png "BFGS 2D") | ![IMGBFGS3D](Homework_1/Results/BFGS_3d.png "BFGS 3D")
--- | ---

**Conjugate gradient:**

![IMGBFGS2D](Homework_1/Results/Conjugate_gradient_2d.png "Conjugate gradient 2D") | ![IMGBFGS3D](Homework_1/Results/Conjugate_gradient_3d.png "Conjugate gradient 3D")
--- | ---

Note that these results were obtained using python 3.4.3 (scipy
1.0.0). Different results may be obtained with other versions when running the
same code, e.g. the BFGS method using python 2.7.6 with scipy 0.13.3 yields the
following result:

**BFGS (Different Python/Scipy version):**

<!-- ![IMGBFGS3D](Homework_1/Results/BFGS_2d_py27.png "Conjugate gradient 2D(Python2)") -->
<img src="Homework_1/Results/BFGS_2d_py27.png" alt="Conjugate gradient 2D(Python2)" width="600">

### Homework 2

See /homework2/README.md for instructions.

### Homework 3

### Homework 4

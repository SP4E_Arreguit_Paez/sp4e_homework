""" Conjugate gradient """

import numpy as np
try:
    import sympy as sym
except ImportError:
    print("Warning: Sympy cannot be imported, functions using sympy will fail")


def function_S(x, y):
    """ Function S: S(x, y) = 2*x**2 + 3/2*y**2 + x*y - x - 2*y + 6 """
    return 2.*x**2 + 3./2.*y**2 + x*y - x - 2.*y + 6.


def function_S_components():
    """ Return A and b of function_S when written as qudratic function """
    A = np.array([[4, 1], [1, 3]])
    b = np.array([1, 2])
    return A, b


def quadratic_function_symbolic_numpy(x, A, b):
    """ Quaratic function 1/2*x^T*A*x - x^T*b """
    xt = x.transpose()
    return 0.5*np.dot(xt, A).dot(x) - np.dot(xt, b)


def quadratic_function(x, A, b):
    """ Quaratic function 1/2*x^T*A*x - x^T*b """
    return 0.5*np.einsum("j, ij, i", x, A, x) - np.einsum("i, i", x, b)


def linear_system_error_numpy(x, A, b):
    """ Compute error of linear system err = A*x - b """
    return np.dot(A, x) - b


def linear_system_error(x, A, b):
    """ Compute error of linear system err = A*x - b """
    return np.einsum("ij, i", A, x) - b


def conjgrad_numpy(A, b, x, tol=1e-6, callback=None):
    """ Conjugate gradient """
    r = b - np.dot(A, x)
    p = r
    rsold = np.dot(r, r)
    for i, bi in enumerate(b):
        Ap = np.dot(A, p)
        alpha = rsold / np.dot(p, Ap)
        x = x + alpha * p
        r = r - alpha * Ap
        rsnew = np.dot(r, r)
        if callback is not None:
            callback(x)
        if np.sqrt(rsnew) < 1e-10:
            break
        p = r + (rsnew / rsold) * p
        rsold = rsnew
    return x


def conjgrad(A, b, x, tol=1e-10, callback=None):
    """ Conjugate gradient with Einstein notation """
    r = b - np.einsum("ij, i", A, x)
    p = r
    rsold = np.einsum("i, i", r, r)
    for i, bi in enumerate(b):
        Ap = np.einsum("ij, i", A, p)
        alpha = rsold / np.einsum("i, i", p, Ap)
        x = x + alpha * p
        r = r - alpha * Ap
        rsnew = np.einsum("i, i", r, r)
        if callback is not None:
            callback(x)
        if np.sqrt(rsnew) < tol:
            break
        p = r + (rsnew / rsold) * p
        rsold = rsnew
    return x


def quadratic_function_symbolic():
    """ Quadratic function using sympy """
    # Sympy
    a11, a12, a21, a22 = sym.symbols(["a11, a12, a21, a22"])[0]
    b1, b2 = sym.symbols(["b1, b2"])[0]
    x1, x2 = sym.symbols(["x1, x2"])[0]
    A = np.array([[a11, a12], [a21, a22]])
    b = np.array([b1, b2])
    x = np.array([x1, x2])
    res = quadratic_function_symbolic_numpy(x, A, b)
    print(res)  # a11=4, a12=1, a21=1, a22=3, b1=1, b2=2
    return res


def quadratic_function_symbolic_check(A, b):
    """ Quadratic function using sympy """
    # Sympy
    x1, x2 = sym.symbols(["x, y"])[0]
    x = np.array([x1, x2])
    res = quadratic_function_symbolic_numpy(x, A, b)
    print(res)
    return res


def test():
    """ Test """
    N = 3
    x = 3*np.ones(N)
    b = np.ones(N)
    A = np.eye(N)
    res = quadratic_function(x, A, b)
    print("quaratic_function(x, A, b) = {}".format(res))
    err = linear_system_error(x, A, b)
    print("linear_system_error(x, A, b) = {}".format(err))
    x_correct = conjgrad(A, b, x)
    print("Correct value of x: {}".format(x_correct))
    err = linear_system_error(x_correct, A, b)
    print("linear_system_error(x_correct, A, b) = {}".format(err))
    A = np.array([[4, 1], [1, 3]])
    b = np.array([1, 2])
    quadratic_function_symbolic_check(A, b)
    return


def exercise2(x0=np.zeros(2), callback=None):
    """ Main - Exercise 2 """
    A = np.array([[4, 1], [1, 3]])
    b = np.array([1, 2])
    print("Solving S(x,y):")
    x_correct = conjgrad(A, b, x0, callback=callback)
    print("Correct value of (x, y): {}".format(x_correct))
    err = linear_system_error(x_correct, A, b)
    print("linear_system_error(x_correct, A, b) = {}".format(err))
    print("S(x_f, y_f) = {}".format(function_S(x_correct[0], x_correct[1])))
    return x_correct


if __name__ == "__main__":
    exercise2()

import numpy as np
from scipy.optimize import minimize
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


l = []
result_iter = []


def reset_results():
    """ Reset results """
    global l
    global result_iter
    l = []
    result_iter = []
    return


def append_result(x):
    """ Reset results """
    global l
    global result_iter
    l.append(x)
    result_iter.append(s_fun(x))
    return


def minimizer(s, x0=np.zeros(2), method="BFGS"):
    append_result(x0)

    # return minimize(s,x0,method='Nelder-Mead',callback=getIterationSteps)
    return minimize(s, x0, method=method, callback=getIterationSteps)


def s_fun(x):
    return 2*x[0]**2+(3./2)*x[1]**2+x[0]*x[1]-x[0]-2*x[1]+6


def getIterationSteps(x):
    # x contains the solution at each step
    print (x[0], x[1])
    append_result(x)
    return


def plot_function_S_surface(extent):
    """ Plot surface

    Creates grid in order to print 3D the contour
    """
    fig = plt.gcf()
    size = 100
    x = np.linspace(extent[0], extent[1], size)
    y = np.linspace(extent[2], extent[3], size)
    X, Y = np.meshgrid(x, y)
    Z = s_fun(np.array([X, Y]))
    axe = Axes3D(fig)
    plt.contour(X, Y, Z, 10)
    ax = plt.gca()
    ax.plot_surface(X, Y, Z, alpha=0.2)
    axe.set_xlabel("X")
    axe.set_ylabel("Y")
    axe.set_zlabel("Z")
    return


def get_plot_data(x0=None):
    """ Get plot_data """
    global l
    l_array = np.array(l)
    xf = l_array[-1, :]
    s = np.linalg.norm(xf - x0) if x0 is not None else 0.5
    s2 = 0.5
    extent = (
        xf[0]-s-s2,
        xf[0]+s+s2,
        xf[1]-s-s2,
        xf[1]+s+s2
    )
    return l_array, extent


def plot2D(method="", x0=None):
    """ Plot results in 2D """
    # plot 2D iterations x & y
    l_array, extent = get_plot_data(x0=x0)
    x = np.linspace(extent[0], extent[1], 1e3)
    y = np.linspace(extent[2], extent[3], 1e3)
    X, Y = np.meshgrid(x, y)
    Z = s_fun(np.array([X, Y]))
    im = plt.imshow(
        Z,
        # cmap=plt.cm.RdBu,
        extent=extent
    )
    cset = plt.contour(
        Z,
        10,
        linewidths=2,
        cmap=plt.cm.Set2,
        extent=(extent[0], extent[1], extent[3], extent[2])
    )
    plt.clabel(cset, inline=True, fmt='%1.1f', fontsize=10)
    plt.colorbar(im)
    plt.plot(
        l_array[:, 0], l_array[:, 1],
        "ro-", linewidth=2, markersize=5,
        label="{}".format(method)
    )
    plt.legend()
    plt.xlim(extent[:2])
    plt.ylim(extent[2:])
    return


def plot3D(x0=None):
    """ Plots results in 3D """
    l_array, extent = get_plot_data(x0)

    # Plot S function
    plot_function_S_surface(extent)

    # prints the results of the iterations in 3D
    plt.plot(l_array[:, 0], l_array[:, 1], result_iter, '-ro')
    return


def exercise1(x0=np.zeros(2)):
    """ Exercise 1 """
    method = "BFGS"

    # Run minimizer
    minimizer(s_fun, x0=x0, method=method)

    # 2D plot
    plt.figure("{} 2D".format(method))
    plot2D(method=method, x0=x0)
    plt.savefig("Results/{}_2d.png".format(method))

    # 3D plot
    plt.figure("{} 3D".format(method))
    plot3D(x0=x0)
    plt.savefig("Results/{}_3d.png".format(method))
    return


if __name__ == "__main__":
    # https://people.duke.edu/~ccc14/sta-663/BlackBoxOptimization.html
    exercise1()
    plt.show()

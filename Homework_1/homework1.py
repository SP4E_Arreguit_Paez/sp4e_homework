from optimizer import (
    exercise1,
    getIterationSteps,
    reset_results,
    append_result,
    plot2D,
    plot3D
)
from conjugate_gradient import exercise2

import numpy as np
import matplotlib.pyplot as plt


def main():
    """ Main """
    x0 = np.zeros(2)

    # Exercise 1
    print("\nExercise 1:\n===========")
    reset_results()
    exercise1(x0=x0)

    # Exercise 2
    print("\nExercise 2:\n===========")
    reset_results()
    method = "Conjugate gradient"
    append_result(x0)
    exercise2(x0=x0, callback=getIterationSteps)
    plt.figure("{} 2D".format(method))
    plot2D(method, x0=x0)
    plt.savefig("Results/{}_2d.png".format(method).replace(" ", "_"))
    plt.figure("{} 3D".format(method))
    plot3D(x0=x0)
    plt.savefig("Results/{}_3d.png".format(method).replace(" ", "_"))
    return


if __name__ == "__main__":
    main()
    plt.show()

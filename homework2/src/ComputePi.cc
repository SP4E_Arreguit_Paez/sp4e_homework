#include <ComputePi.hh>

double ComputePi::compute(unsigned int N) {
    Series::compute(N);
    return sqrt(6*current_value);
}

double ComputePi::getAnalyticPrediction() {
    return M_PI;
}

double ComputePi:: computeTerm(uint k) {
    return 1./(k*k);
}

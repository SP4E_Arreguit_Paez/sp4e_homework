#include <iostream>
#include <ComputeArithmetic.hh>
#include <ComputePi.hh>
#include <Series.hh>
#include <print_series.hh>
#include <write_series.hh>
#include <fstream>
#include <memory>


// Print series to cout
void print_series(std::shared_ptr<Series> series, uint maxiter, uint freq, uint precision=5)
{
    PrintSeries print(*series, maxiter, freq);
    print.setPrecision(precision);
    print.dump(std::cout);
    return;
}

// Write series to file filename
void write_series(std::shared_ptr<Series> series, uint maxiter, std::string filename, uint precision=5)
{
    WriteSeries write(*series, maxiter);
    write.setPrecision(precision);
    std::string extension = filename.substr(filename.find_last_of(".") + 1);
    if (extension == "csv")
        write.setSeparator(",");
    else if (extension == "txt")
        write.setSeparator(" ");
    else if (extension == "psv")
        write.setSeparator("|");
    else
    {
        std::cerr << "Unrecognised format " << extension << std::endl;
        std::exit(0);
    }
    std::ofstream out;
    std::cout << "Writing to file " << filename << std::endl;
    out.open(filename);
    write.dump(out);
    out.close();
    return;
}

// Test series printing and writing to all formats
void test_series(std::shared_ptr<Series> series, uint maxiter, uint freq, std::string filename="output")
{
    std::cout << "- Testing print..." << std::endl;
    print_series(series, maxiter, freq);
    std::cout << "- Testing print DONE" << std::endl;
    std::cout << "- Testing write..." << std::endl;
    write_series(series, maxiter, filename+".csv");
    write_series(series, maxiter, filename+".txt");
    write_series(series, maxiter, filename+".psv");
    std::cout << "- Testing write DONE" << std::endl;
    return;
}

// Test arithmetic series printing and writing to all formats
void test_arithmetic(uint maxiter, uint freq)
{
    std::cout << "- TESTING ARITHMETIC SERIES..." << std::endl;
    std::shared_ptr<Series> series = std::make_shared<ComputeArithmetic>();
    test_series(series, maxiter, freq, "output_arithmetic");
    std::cout << "- TESTING ARITHMETIC SERIES DONE" << std::endl;
    return;
}

// Test pi series printing and writing to all formats
void test_pi(uint maxiter, uint freq)
{
    std::cout << "- TESTING PI SERIES..." << std::endl;
    std::shared_ptr<Series> series = std::make_shared<ComputePi>();
    test_series(series, maxiter, freq, "output_pi");
    std::cout << "- TESTING PI SERIES DONE" << std::endl;
    return;
}

// Test arithmetic and pi series printing and writing to all formats
void test(uint maxiter, uint freq)
{
    test_arithmetic(maxiter, freq);
    test_pi(maxiter, freq);
    return;
}

int main(int argc, char ** argv){

    // Check that correct number of arguments were given
    if (argc<4){
        std::cerr << "Wrong Number of arguments: " << argc << std::endl;
        std::cerr << "Program should be called with:" << std::endl;
        std::cerr
            << std::endl
            << "  homework2 series_name (pi/arithmetic/test) #maxiter #freq"
            << std::endl
            << std::endl;
        std::exit(0);
    }

    //stores the user input information in variables:
    std::string series_name = std::string(argv[1]);
    int maxiter = atoi(argv[2]);
    int freq = atoi(argv[3]);

    //Check the correct input parameters otherwise the program finishes
    if (series_name != "arithmetic" && series_name != "pi" && series_name != "test"){
        std::cerr
            << "Wrong series name: "
            << series_name
            << " The options are 'arithmetic', 'pi' or 'test'"
            << std::endl;
        std::exit(0);
    }

    //Print selected argument values
    std::cout << "Series type chosen is: " << series_name << std::endl;
    std::cout << "maxiter chosen values is: " << maxiter << std::endl;
    std::cout << "Freq chosen value is: " << freq << std::endl;

    //create series object according to user selection
    // std::shared_ptr<Base> p = std::make_shared<Derived>();
    std::shared_ptr<Series> series;
    if(series_name == "arithmetic")
        series = std::make_shared<ComputeArithmetic>();
    else if (series_name == "pi")
        series = std::make_shared<ComputePi>();
    else if (series_name == "test")
    {
        test(maxiter, freq);
        return 0;
    }
    else
    {
        std::cerr << "Unknown series of type " << series_name << std::endl;
        return 1;
    }

    // Test PrintSeries class
    print_series(series, maxiter, freq);

    // Test WriteSeries class
    write_series(series, maxiter, "output.csv");

    return 0;
}




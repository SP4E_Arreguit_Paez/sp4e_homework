#include <print_series.hh>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <cmath>


using uint = unsigned int;


PrintSeries::PrintSeries(Series & series, uint maxiter, uint freq) : DumperSeries(series)
{
    this->setMaxiter(maxiter);
    this->setFreq(freq);
    return;
}

void PrintSeries::dump(std::ostream & os)
{
    std::stringstream out;
    double res, res2;

    std::setprecision(this->precision);

    if (this->freq) {
        for (int i=1; i < this->maxiter/this->freq; i++) {
            res = this->series.compute(i*this->freq-1);
            res2 = this->series.compute(i*this->freq);
            out << "Step "<< i*this->freq << ": " << res;
            out << " (change: " << res2 - res;
            out << ", convergence: ";
            out << std::abs(res2 - this->series.getAnalyticPrediction());
            out << ")" << std::endl;
        }
    } else {
        out << "WARNING: Frequency set to zero, nothing to print" << std::endl;
    }
    std::cout << out.str();
    return;
}

void PrintSeries::setMaxiter(uint maxiter)
{
    this->maxiter = maxiter;
    return;
}

void PrintSeries::setFreq(uint freq)
{
    this->freq = freq;
    return;
}

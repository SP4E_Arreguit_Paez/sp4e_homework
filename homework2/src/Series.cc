#include "Series.hh"
#include <cmath>

using uint = unsigned int;

Series::Series() {
    this->current_index = 0;
    this->current_value = 0;
}

double Series::compute(uint N) {
    if (this->current_index <= N)
    {
        N -= this->current_index;
    }
    else
    {
        this->current_value = 0.;
        this->current_index = 0;
    }
    for (uint k=0; k < N; k++) {
        this->addTerm();
    }
    return this->current_value;
}

void Series::addTerm() {
    this->current_index++;
    this->current_value += this->computeTerm(this->current_index);
}

double Series::getAnalyticPrediction() {
    return nan("");
}

double Series::compute_internal_value(int i) {

}

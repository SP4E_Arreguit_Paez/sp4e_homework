#include <dumper_series.hh>

DumperSeries::DumperSeries(Series &_series) : series(_series)
{
    this->precision = 4;
    return;
}

void DumperSeries::setPrecision(unsigned int precision)
{
    this->precision = precision;
    return;
}

#include <write_series.hh>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using uint = unsigned int;

WriteSeries::WriteSeries(Series & series, uint maxiter, std::string separator) : DumperSeries(series)
{
    this->setMaxiter(maxiter);
    return;
}

void WriteSeries::dump(std::ostream & out)
{
    double res, res2;

    std::setprecision(this->precision);

    out
        << "# Step "
        << this->separator
        << " Value "
        << this->separator
        << " Convergence"
        << std::endl;
    for (int i=1; i < this->maxiter; i++) {
        res = this->series.compute(i-1);
        res2 = this->series.compute(i);
        out
            << i
            << this->separator
            << res
            << this->separator
            << std::abs(res2 - this->series.getAnalyticPrediction())
            << std::endl;
    }
    return;
}

void WriteSeries::setMaxiter(uint maxiter)
{
    this->maxiter = maxiter;
    return;
}

void WriteSeries::setSeparator(std::string separator)
{
    if (
        separator == ","
        || separator == " "
        || separator == "\t"
        || separator == "|"
        )
    {
        this->separator = separator;
    }
    else
    {
        std::cerr << "ERROR: Unrecognised separator" << std::endl;
        std::exit(0);
    }
    return;
}

# Homework 2

## Original work division

- Laura
  - Exercise 1: Creation of project structure
  - Exercise 2: Series class
  - Exercise 3.4: addition to Series Class
  - Exercise 5: Modification to Series Class
  - Merge codes with exercises 3 and 4
- Jonathan
  - Exercise 3: Dumper class
  - Exercise 4: std::ostream manipulation
- Collaboration
  - Exercise 5: Series complexity

## Installation instructions

Compile the controller:

1. `cd PATH_TO_SP4E_Arreguit_Paez/homework2/`
2. `mkdir build`
3. `cd build/`
4. `cmake ..`
5. `make`

## Run instructions

In order to run the homework, you can for example run the following command line:

`homework2 pi 10 1`

Note that you may need to move to change the directory to the appropriate folder
to run the executable. By default and if the instructions above were followed,
this will be inside _/PATHTOREPO/homework2/build/src/_. This will run the pi
series with 10 iterations and print every step (freq = 1). To obtain more
information about how to run the program, you can run `homework2` in your
terminal without arguments. In order to run the program in test mode, where the
majority of the code is tested, you can simply run:

`homework2 test #maxiter #freq`

where _#maxiter_ and _#freq_ are to be replaced by the number of iterations and
the frequency at which to output the data respectively. In order to visualise
the results, a python script _plot.py_ has been implemented and can be found in
the _Python/_ folder. You can run the following command to obtain more
information about how to correctly run the program:

`python plot.py --help`

If the homework2 was compiled and run in test mode, a shell script _plot.sh_ in
the -python/- folder is also provided to generate all the plots
automatically. If your terminal supports running shell scripts, you could run
it as follows:

`sh plot.sh`

The script will automatically generate the plots for all the outputs generated
by the homework2 executable (important: in test mode) and will save the files
inside the _results/_ folder. Note that the script currently expects the results
to be found inside the build folder, which is what is obtained automatically if
running the following command from the build folder
_/PATHTOREPO/homework2/build/_ for example, otherwise the script may need to be
modified appropriately.

`./src/homework2 test 30 1`

## Results

**Arithmetic series**

![IMGARITHMETICSERIES](results/arithmetic.png "Arithmetic series")

**PI series**

![IMGPISERIES](results/pi.png "Pi series") |
![IMGPICONVERGENCE](results/pi_convergence.png "Pi series convergence")
--- | ---

## Complexity

Originally, the code would recompute the entire series every time the code is
run to find out the value $`S_n`$ of a series at step $`N`$ as follows:

```math
S_N = \sum_{k=1}^{N} s_k
```

This is computationally expensive and inefficient when the code is rerun many
times, especially in the case where a user would like to print each of the steps
to a certain number. Indeed, the complexity of the program would be $`O(N)`$. In
order to improve the complexity, we can change the formulation to the following:

```math
S_{N+1} = S_{N} + s_{N+1}
```

This means that the index $`N`$ and and the value $`S_{N}`$ must be stored at every
step such that the series can be recomputed from step $`N`$ in the case where the
values of the series at a new step $`M > N`$ is requested, thus reducing the total
computation necessary when the code is called in a for-loop as it avoids
recomputing from $`k=1`$.

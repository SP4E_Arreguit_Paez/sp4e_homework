""" Plot series """

import argparse
import numpy as np
import matplotlib.pyplot as plt


def parse_cl_arguments():
    """ Parse command line arguments """
    parser = argparse.ArgumentParser(
        description='Plot series from homework2 output'
    )
    parser.add_argument(
        "filename", type=str,
        help="File containing the series output to be plotted"
    )
    parser.add_argument(
        "-t, ""--title", type=str,
        dest='title',
        default=None,
        help="Title of plot"
    )
    parser.add_argument(
        "-s, ""--save", type=str,
        dest='save',
        default=None,
        help="File to which to save plot"
    )
    parser.add_argument(
        "-c, ", "--convergence",
        dest="convergence",
        action="store_true",
        help="Plot convergence"
    )
    parser.add_argument(
        "-n, ", "--notshow",
        dest="notshow",
        action="store_true",
        help="Option to not show plots"
    )
    args = parser.parse_args()
    extension = args.filename.split(".")[-1]
    delimiter = (
        "," if extension == "csv"
        else " " if extension == "txt"
        else "|"
    )
    return args, delimiter


def load_output(filename, delimiter):
    """ Load data from series output """
    return np.loadtxt(filename, comments="#", delimiter=delimiter)


def plot_ouput(data, title=None, save=None):
    """ Plot data from series output """
    # Series
    plt.figure(title if title else "Series")
    plt.plot(data[:, 0], data[:, 1])
    plt.xlabel("Iterations")
    plt.ylabel("Series value")
    plt.grid(True)
    if title:
        plt.title(title)
    if save:
        plt.savefig(save)
    return


def plot_ouput_convergence(data, title=None, save=None):
    """ Plot data from series output """
    # Convergence
    plt.figure("{} convergence".format(title) if title else "Convergence")
    plt.plot(data[:, 0], data[:, 2])
    plt.xlabel("Iterations")
    plt.ylabel("Error")
    plt.grid(True)
    if title:
        plt.title("{} convergence".format(title))
    if save:
        save = save.split(".")
        save[-2] = save[-2]+"_convergence"
        save = ".".join(save)
        plt.savefig(save)
    return


def main():
    """ Main """
    # Parse command line arguments
    cl_args, delimiter = parse_cl_arguments()
    filename = cl_args.filename
    print(
        "Series output plotting tool:"
        + " Loading file {} with delimiter '{}'".format(filename, delimiter)
        + " to plot data"
    )
    data = load_output(filename, delimiter)
    plot_ouput(data, title=cl_args.title, save=cl_args.save)
    if cl_args.convergence:
        plot_ouput_convergence(data, title=cl_args.title, save=cl_args.save)
    if not cl_args.notshow:
        plt.show()
    return


if __name__ == '__main__':
    main()

# Make sure to first run homework2 in test mode before running this file
python3 plot.py "../build/output_arithmetic.csv" -t "Arithmetic series" -s "../results/arithmetic.png" --notshow
python3 plot.py "../build/output_pi.csv" -t "Pi series" -s "../results/pi.png" --notshow --convergence

python3 plot.py "../build/output_arithmetic.txt" -t "Arithmetic series (txt)" -s "../results/arithmetic_txt.png" --notshow
python3 plot.py "../build/output_pi.txt" -t "Pi series (txt)" -s "../results/pi_txt.png" --notshow --convergence

python3 plot.py "../build/output_arithmetic.psv" -t "Arithmetic series (psv)" -s "../results/arithmetic_psv.png" --notshow
python3 plot.py "../build/output_pi.psv" -t "Pi series (psv)" -s "../results/pi_psv.png" --notshow --convergence

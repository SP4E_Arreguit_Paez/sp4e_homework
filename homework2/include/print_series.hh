#ifndef PRINT_SERIES_H
#define PRINT_SERIES_H

// Print series

#include <dumper_series.hh>
#include <iostream>
#include <Series.hh>

class PrintSeries : public DumperSeries
{
public:
    PrintSeries(Series & series, uint maxiter=1e2, uint freq=1);
    virtual ~PrintSeries() {};

public:
    void setMaxiter(uint maxiter);
    void setFreq(uint freq);
    void dump(std::ostream & os = std::cout) override;

private:
    uint maxiter;
    uint freq;
};

#endif /* PRINT_SERIES_H */

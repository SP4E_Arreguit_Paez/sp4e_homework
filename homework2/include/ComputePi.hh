#include <Series.hh>
#include <math.h>

class ComputePi : public Series{

public:
    ComputePi() : Series() {};
    virtual ~ComputePi() {};

public:
    double compute(uint N);
    double computeTerm(uint k);
    double getAnalyticPrediction();
};

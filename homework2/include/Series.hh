#ifndef SERIES_H
#define SERIES_H
#include <iostream>

using uint = unsigned int;

class Series{
public:
    Series();
    virtual double compute(uint N) = 0;
    virtual double getAnalyticPrediction();
    virtual double compute_internal_value(int i);
    virtual double computeTerm(uint k) = 0;

    uint current_index;
    double current_value;

private:
    virtual void addTerm();
};

#endif

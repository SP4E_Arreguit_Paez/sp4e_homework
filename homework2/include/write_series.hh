#ifndef WRITE_SERIES_H
#define WRITE_SERIES_H

// Write series

#include <dumper_series.hh>
#include <iostream>
#include <Series.hh>

class WriteSeries : public DumperSeries
{
public:
    WriteSeries(Series & series, uint maxiter=1e2, std::string separator=" ");
    virtual ~WriteSeries() {};

public:
    void setMaxiter(uint maxiter);
    void setSeparator(std::string separator);
    void dump(std::ostream & os) override;

private:
    uint maxiter;
    std::string separator;
};

#endif /* WRITE_SERIES_H */

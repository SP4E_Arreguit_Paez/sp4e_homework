#ifndef DUMPER_SERIES_H
#define DUMPER_SERIES_H

// Dumper series

#include <ostream>
#include <Series.hh>

class DumperSeries{

public:
    DumperSeries(Series & series);
    virtual ~DumperSeries() {};

public:
    virtual void dump(std::ostream & os) = 0;
    virtual void setPrecision(uint precision);

protected:
    uint precision;
    Series & series;
};

inline std::ostream & operator <<(std::ostream & stream, DumperSeries & _this) {
    _this.dump(stream);
    return stream;
}

#endif /* DUMPER_SERIES_H */

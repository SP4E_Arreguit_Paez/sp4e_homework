#include <Series.hh>

class ComputeArithmetic : public Series {

public:
    ComputeArithmetic(): Series() {};
    virtual ~ComputeArithmetic() {};

public:
    double compute(uint N);
    double computeTerm(uint k);
};
